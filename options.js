//const usernameInput = document.querySelector("#username");
//const passwordInput = document.querySelector("#password");
const urlInput = document.querySelector("#url");
const listsInput = document.querySelector("#lists");
const tagsInput = document.querySelector("#tags");

/*
Store the currently selected settings using browser.storage.local.
*/
function storeSettings() {
  browser.storage.local.set({
    authCredentials: {
      //username: usernameInput.value,
      //password: passwordInput.value,
      url: urlInput.value,
      lists: listsInput.value,
      tags: tagsInput.value
    }
  });
}

/*
Update the options UI with the settings values retrieved from storage,
or the default settings if the stored settings are empty.
*/
function updateUI(restoredSettings) {
  //usernameInput.value = restoredSettings.authCredentials.username || "";
  //passwordInput.value = restoredSettings.authCredentials.password || "";
  urlInput.value = restoredSettings.authCredentials.url || "";
  listsInput.value = restoredSettings.authCredentials.lists || "";
  tagsInput.value = restoredSettings.authCredentials.tags || "";
}

function onError(e) {
  console.error(e);
}

/*
On opening the options page, fetch stored settings and update the UI with them.
*/
const gettingStoredSettings = browser.storage.local.get();
gettingStoredSettings.then(updateUI, onError);

/*
On blur, save the currently selected settings.
*/
//usernameInput.addEventListener("blur", storeSettings);
//passwordInput.addEventListener("blur", storeSettings);
urlInput.addEventListener("blur", storeSettings);
listsInput.addEventListener("blur", storeSettings);
tagsInput.addEventListener("blur", storeSettings);
