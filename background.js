var currentTab;


function updateActiveTab(tabs) {
    function updateTab(tabs) {
        if (tabs[0]) {
          currentTab = tabs[0];
        }
    }

    var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});
    gettingActiveTab.then(updateTab);
}


function bookmark() {
    var url = currentTab.url;
    var title = currentTab.title;
    // OMG won't do it myself
    var description=document.getSelection()||'';
    
    var encodedURL = encodeURIComponent(url);
    var encodedTitle = encodeURIComponent(title);
    var encodedDescription = encodeURIComponent(description);

    var gettingUpdatedStoredSettings = browser.storage.local.get();
    gettingUpdatedStoredSettings.then((config) => {
        //console.log(config['authCredentials']);
        linkaceURL = config['authCredentials']['url']
        linkaceLists = config['authCredentials']['lists']
        linkaceTags = config['authCredentials']['tags']

        //console.log(linkaceURL);
        var destUrl = linkaceURL + "/bookmarklet/add?u=" + encodedURL + '&t=' + encodedTitle + '&d=' + encodedDescription;
        if (linkaceLists) {
            destUrl = destUrl + '&lists=' + linkaceLists;
        }
        if (linkaceTags) {
            destUrl = destUrl + '&tags=' + linkaceTags;
        }
        browser.windows.create({allowScriptsToClose: true, url: destUrl, height: 620, width: 600, type: "popup"});
    });
}
// listen to click
browser.browserAction.onClicked.addListener(bookmark);


// listen to tab URL changes
browser.tabs.onUpdated.addListener(updateActiveTab);
// listen to tab switching
browser.tabs.onActivated.addListener(updateActiveTab);
// listen for window switching
browser.windows.onFocusChanged.addListener(updateActiveTab);
